String.prototype.startWith = function (str) {
	if (str == null || str == "" || this.length == 0 || str.length > this.length)
		return false;
	if (this.substr(0, str.length) == str)
		return true;
	else
		return false;
	return true;
}

var blockUid = 561214;

var blockStr = '**** 已被屏蔽 ****';

// 处理博客中的评论
if (window.location.host == 'my.oschina.net') {
	var hrefs = window.location.href.split('/');
	if (hrefs.length >= 6) {
		// 获取BlogId
		var blogId = hrefs[5].split('?')[0];
		// 获取其评论DOM
		var commentDom = $('li[id^=cmt_' + blogId + '_' + blockUid + '_]');
		// 找到其评论，进行处理
		if (commentDom.length > 0) {
			// 移除其回复评论功能
			commentDom.find("a[ng-if='commentjson.canCommit']").remove();
			// 清理其头像
			commentDom.find('td.portrait').find('a').remove();
			// 处理其名字
			commentDom.find("a[ng-bind='commentjson.userName']").removeAttr('ng-href').removeAttr('target').removeAttr('name').removeAttr('href').text(blockStr);
			// 处理其评论
			commentDom.find("div[ng-bind-html='commentjson.content']").text(blockStr);
		}
	}
}
// 处理新闻中的评论
else if (window.location.href.startWith('http://www.oschina.net/news/')) {
	var hrefs = window.location.href.split('/');
	if (hrefs.length >= 6) {
		// 获取NewsId
		var newsId = hrefs[4].split('?')[0];
		// 获取其评论DOM
		var commentDom = $('li[id^=cmt_' + newsId + '_' + blockUid + '_]');
		// 找到其评论，进行处理
		if (commentDom.length > 0) {
			// 移除其回复评论功能
			commentDom.find('span.opts').remove();
			// 清理其头像
			commentDom.find('td.portrait').find('a').remove();
			// 处理其名字
			commentDom.find('div.r_title').find('b').text(blockStr);
			// 处理其评论
			commentDom.find("div[class='r_content TextContent']").text(blockStr);
		}
	}
}
// 处理问答中的评论
else if (window.location.href.startWith('http://www.oschina.net/question/')) {
	// 获取其头像DOM
	var imgDom = $("img[user='" + blockUid + "']");
	// 获取其评论DOM
	var commentDom = imgDom.parent().parent().parent().parent();
	// 找到其回答，进行处理
	if (commentDom.length > 0) {
		// 移除其回复功能
		commentDom.find('span.a_toolbar').find('a')[0].remove();
		// 移除其引用功能
		commentDom.find('span.a_toolbar').find('a')[0].remove();
		// 清理其头像
		imgDom.parent().remove();
		// 处理其名字
		commentDom.find('span.user_info').find('a').removeAttr('href').removeAttr('title').text(blockStr);
		// 处理其评论
		commentDom.find('div.detail').text(blockStr);
	}
}
// 处理翻译中的评论
else if (window.location.href.startWith('http://www.oschina.net/translate/')) {
	// 获取其头像DOM
	var imgDom = $("img[user='" + blockUid + "']");
	// 获取其评论DOM
	var commentDom = imgDom.parent().parent().parent();
	// 找到其回答，进行处理
	if (commentDom.length > 0) {
		// 移除其回复功能
		commentDom.find('div.opts').remove();
		// 清理其头像
		imgDom.parent().remove();
		// 处理其名字
		commentDom.find('div.time').find('b').text(blockStr);
		// 处理其评论
		commentDom.find('div.detail').text(blockStr);
	}
}
// 处理代码分享中的评论
else if (window.location.href.startWith('http://www.oschina.net/code/')) {
	// 获取其评论DOM
	var commentDom = $("li[id^=cmt_" + blockUid + "_]");
	// 找到其回答，进行处理
	if (commentDom.length > 0) {
		// 移除其回复功能
		commentDom.find('span.opts').remove();
		// 清理其头像
		commentDom.find('td.portrait').find('a').remove();
		// 处理其名字
		commentDom.find('div.r_title').find('b').text(blockStr);
		// 处理其评论
		commentDom.find("div[class='r_content TextContent']").text(blockStr);
	}
}
